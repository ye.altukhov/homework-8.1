# homework-8.1

Run docker composer up -d to launch percona and mysql container.

SET GLOBAL innodb_status_output=ON;
SET GLOBAL innodb_status_output_locks=ON;

Import sample data from ./data/users.sql

Run mysql -uroot -p123456 -h127.0.0.1 -P3306 homework-8.1 to connect to the database.
And 
Run mysql -uroot -p123456 -h127.0.0.1 -P3306 homework-8.1-mysql to connect to the database.

Phantom read problem:

Mysql and percona behaves the same:
I have a table of 10 users. Insert a user with id = 13 to create a gap between user ids in both dbs.

INSERT INTO Users (id, name, surname, email, birthday)
VALUES (13, 'Bob', 'Wawes', 'bob@gmail.com', '2001-01-01');

Set session transaction isolation level to Read Commited and exeute a select query to get users with user_id > 9
SET SESSION TRANSACTION ISOLATION LEVEL READ COMMITTED;
START TRANSACTION;
SELECT * FROM Users WHERE id > 9;
Percona
![nr read](./imgs/Screenshot 2022-09-26 at 12.26.17.png "nr read")
Mysql
![nr read](./imgs/Screenshot 2022-09-26 at 12.26.20.png "nr read")


In another transaction insert a user with id = 11  insert in the gap.
INSERT INTO Users (id, name, surname, email, birthday)
VALUES (11, 'Elon', 'Mask', 'elon@gmail.com', '1971-06-28');
Percona
![nr read](./imgs/Screenshot 2022-09-26 at 12.29.00.png "nr read")

Mysql
![nr read](./imgs/Screenshot 2022-09-26 at 12.29.09.png "nr read")

Non-repeatable read problem

Mysql and percona behaves the same:

SET SESSION TRANSACTION ISOLATION LEVEL READ COMMITTED;
START TRANSACTION;
SELECT * FROM Users WHERE id = 4;
Percona
![nr read](./imgs/Screenshot 2022-09-26 at 12.30.17.png "nr read")
Mysql
![nr read](./imgs/Screenshot 2022-09-26 at 12.30.22.png "nr read")

in next transaction:

UPDATE Users SET name = 'Lora' WHERE id = 4;
SELECT * FROM Users WHERE id = 4;
Percona
![nr read](./imgs/Screenshot 2022-09-26 at 12.31.11.png "nr read")
MYSQL
![nr read](./imgs/Screenshot 2022-09-26 at 12.31.14.png "nr read")

Dirty read problem

SET SESSION TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
START TRANSACTION;
SELECT * FROM Users WHERE id = 2;

Percona
![nr read](./imgs/Screenshot 2022-09-26 at 12.47.33.png "nr read")
Mysql
![nr read](./imgs/Screenshot 2022-09-26 at 12.47.37.png "nr read")

Update user.name in other transaction but don't do the commit
START TRANSACTION;
UPDATE Users SET name = 'Dan' WHERE id = 2;

SELECT * FROM Users WHERE id = 2;
Percona
![nr read](./imgs/Screenshot 2022-09-26 at 12.51.25.png "nr read")
Mysql
![nr read](./imgs/Screenshot 2022-09-26 at 12.51.28.png "nr read")

Name was updated, but not commited;

Lost update problem:

SET SESSION TRANSACTION ISOLATION LEVEL REPEATABLE READ;
START TRANSACTION;
SELECT * FROM Users WHERE birthday > DATE('2000-01-01');
Percona
![nr read](./imgs/Screenshot 2022-09-26 at 12.55.46.png "nr read")
Mysql
![nr read](./imgs/Screenshot 2022-09-26 at 12.55.42.png "nr read")

In next transaction update birthday for 1 users. I removed 1 user from this range.

UPDATE Users SET birthday = '1999-12-31' WHERE id = 5;
SELECT * FROM Users WHERE birthday > DATE('2000-01-01');
Percona
![nr read](./imgs/Screenshot 2022-09-26 at 12.59.26.png "nr read")
Mysql
![nr read](./imgs/Screenshot 2022-09-26 at 12.59.23.png "nr read")

So we have updated range of users;
