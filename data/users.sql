CREATE TABLE IF NOT EXISTS Users (
    id int NOT NULL AUTO_INCREMENT PRIMARY KEY,
    name varchar(255),
    surname varchar(255),
    email varchar(255) NOT NULL,
   birthday date
);

INSERT INTO Users (id, name, surname, email, birthday) VALUES (1, 'Leanne', 'Graham', 'Sincere@april.biz', '1992-01-03');
INSERT INTO Users (id, name, surname, email, birthday) VALUES (2, 'Ervin', 'Howell', 'Shanna@melissa.tv"', '1998-01-10');
INSERT INTO Users (id, name, surname, email, birthday) VALUES (3, 'Clementine', 'Bauch', 'Nathan@yesenia.net','1984-11-25');
INSERT INTO Users (id, name, surname, email, birthday) VALUES (4, 'Patricia', 'Lebsack', 'Julianne.OConner@kory.org', '1993-12-12');
INSERT INTO Users (id, name, surname, email, birthday) VALUES (5, 'Chelsey', 'Dietrich', 'Lucio_Hettinger@annie.ca', '2001-01-01');
INSERT INTO Users (id, name, surname, email, birthday) VALUES (6, 'Dennis', 'Schulist', 'Karley_Dach@jasper.info', '1994-07-08');
INSERT INTO Users (id, name, surname, email, birthday) VALUES (7, 'Kurtis', 'Weissnat', 'Telly.Hoeger@billy.biz', '1956-01-01');
INSERT INTO Users (id, name, surname, email, birthday) VALUES (8, 'Nicholas', 'Runolfsdottir', 'Sherwood@rosamond.me', '2002-12-01');
INSERT INTO Users (id, name, surname, email, birthday) VALUES (9, 'Glenna', 'Reichert', 'Chaim_McDermott@dana.io', '1991-03-15');
INSERT INTO Users (id, name, surname, email, birthday) VALUES (10, 'Clementina', 'DuBuque', 'Rey.Padberg@karina.biz', '1997-06-19');


